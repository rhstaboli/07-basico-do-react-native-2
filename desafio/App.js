import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput, FlatList, ScrollView, Button,List, ListItem} from 'react-native'
import Example from './components/Example'

export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			textvalue:[{name:'JohnShown', email:'rafael@gmail.com',tel:'12345678'}],
			contato: [{name:'Rafael', email:'rafael@gmail.com',tel:'12345678'},
			          {name:'Renato', email:'renato@gmail.com',tel:'12346567'},
			          {name:'Gustavo', email:'gustavo@gmail.com',tel:'14725836'}],
		};
		this.handleAddTodoItem = this.handleAddTodoItem.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.handledelTodoItem = this.handledelTodoItem.bind(this)
	  }


	  handledelTodoItem(index){
		for(var i = 0; i < this.state.contato.length; i++){
		  if(this.state.contato[i] == index){
			 delete this.state.contato[i]
		  }
		}
		this.setState({
		  value:this.state.contato
		})
		console.log(this.state.value)
	  }

	  handleAddTodoItem() {
		this.setState({arrayvar:[...this.state.contato, newelement]});
		console.log(this.state.value)
	  }

	  handleChange(e) {
		this.setState({
		  textvalue:e.target.value
		})
	  }
	 
 
	render() {
		return (
			<View>
				<Text>Nome</Text>
				<TextInput placeholder="Nome!" onChangeText={() =>  { this.handleChange }} />
				<Button  title="Inserir Contato" onPress={() => {this.handleAddTodoItem}}/>
				{this.state.contato.map((pessoa,index) => {
                 return (
				 <View>
					<Text>{index} - {pessoa.name} - {pessoa.email} - {pessoa.tel} </Text>
					<Button  title="Deletar Contato" onPress={() => {this.handledelTodoItem(pessoa)}}/>
				 </View>
				) })}
		     </View>
			 
		         
	
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 3,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
})
